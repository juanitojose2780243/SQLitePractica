package pm.facci.alcivarjuan.sqlitepractica;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText editTextCodigo, editTextNombre, editTextPrecio;
    Button buttonIngresar, buttonBuscar, buttonModificar, buttonEliminar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextCodigo = (EditText) findViewById(R.id.editTextCodigo);
        editTextNombre = (EditText) findViewById(R.id.editTextNombre);
        editTextPrecio = (EditText) findViewById(R.id.editTextPrecio);

        buttonIngresar = (Button) findViewById(R.id.buttonIngresar);
        buttonEliminar = (Button) findViewById(R.id.buttonEliminar);
        buttonBuscar = (Button) findViewById(R.id.buttonBuscar);
        buttonModificar = (Button) findViewById(R.id.buttonModificar);

    }
}
